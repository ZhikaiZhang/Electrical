*PADS-LIBRARY-PART-TYPES-V9*

STB45N65M5 TO254P1542X460-3N I ANA 11 1 0 0 0
TIMESTAMP 2018.05.04.03.19.16
"Manufacturer_Name" STMicroelectronics
"Manufacturer_Part_Number" STB45N65M5
"RS Part Number" 7832949
"RS Price/Stock" 
"Allied_Number" 70390681
"Allied Price/Stock" http://www.alliedelec.com/stmicroelectronics-stb45n65m5/70390681/
"Arrow Part Number" STB45N65M5
"Arrow Price/Stock" 
"Description" 
"Datasheet Link" http://www.st.com/web/en/resource/technical/document/datasheet/DM00049184.pdf
"Geometry.Height" 4.6mm
GATE 1 3 0
STB45N65M5
1 0 U G
3 0 U S
4 0 U D_(TAB)

*END*
*REMARK* SamacSys ECAD Model
222003/46826/2.26/3/3/Integrated Circuit
