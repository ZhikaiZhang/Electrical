--search for what??? in spimaster to see what is the question incase you forgot
--cs delay could be a problem, we need to look into that!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--testbench with mrfreceive or mrftransmit framebuffer is where it is stored, 
--frambufferwritedata is actually read as one can see from below in that file,(frambufferwritedata <= LLstatus.read_data, which also goes into crcdata into mrfcrc.vhd)
--but how do one determine the address in the framebuffer?��
--, but realistically it is probably better to test with mrf

--strobe determins whether it is busy or not, and SPIstrobe is determined by CSdelay in this file, 
--do we still need that since now we are doing spi slave????
-- strobe also determins read<=writedata???? In SPImaster, makes no sense
--readdata here shoulllllllld be what is cming from the mrf chip
library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use work.mrf_common.all;
use work.types.all;

--! \brief Implements low-level register operations to the MRF24J40.
entity MRFLowLevel is
	port(
		Reset : in boolean; --! The system reset signal.
		HostClock : in std_ulogic; --! The system clock.
		BusClock : in std_ulogic; --! The SPI bus clock.
		Control : in low_level_control_t; --! The control lines.
		Status : buffer low_level_status_t; --! The status lines.
		CSPin : in std_ulogic; --! The SPI chip select pin.
		ClockOE : buffer boolean; --! The output enable signal for the SPI clock.
		MOSIPin : buffer std_ulogic; --! The SPI MOSI pin.
		MISOPin : in std_ulogic;
		ReceiveInt : out boolean;
		done: buffer std_logic); --! The SPI MISO pin.
end entity MRFLowLevel;

architecture RTL of MRFLowLevel is
	signal ControlLatched : low_level_control_t;

	signal StateMachineBusy : boolean;

	signal SPIStrobe : boolean;
	signal SPIWidth : positive range 1 to 24;
	signal SPIWriteData : std_ulogic_vector(23 downto 0);
	signal SPI_write_new: std_ulogic_vector(7 downto 0);
	signal SPIReadData : std_ulogic_vector(23 downto 0);
	signal SPIBusy : boolean;
	signal miso_en, READY, DOUT_VLD: std_logic;
	--signal done: std_logic;
begin
	-- The main state machine.
	process(HostClock) is
		constant CS_DELAY_TICKS : positive := 4;

		type state_t is (IDLE, ASSERT_CS_WAIT, DATA, PRE_DEASSERT_CS_WAIT);
		variable State : state_t;

		variable CSDelayTicks : natural range 0 to CS_DELAY_TICKS - 1;
	begin
		-- State machine.
		if rising_edge(HostClock) then
			SPIStrobe <= false;
			if Reset then
				State := IDLE;
				CSDelayTicks := 0;
			elsif Control.Strobe then 
				State := ASSERT_CS_WAIT;
				ControlLatched <= Control;
				ControlLatched.Strobe <= false;
				CSDelayTicks := CS_DELAY_TICKS - 1;
			else
				if CSDelayTicks /= 0 then
					CSDelayTicks := CSDelayTicks - 1;
				end if;
				case State is
					when IDLE =>
						null;
					when ASSERT_CS_WAIT =>
						if CSDelayTicks = 0 then
							SPIStrobe <= true;
							State := DATA;
						end if;
					when DATA =>
						if not SPIBusy then
							State := PRE_DEASSERT_CS_WAIT;
							CSDelayTicks := CS_DELAY_TICKS - 1;
						end if;
					when PRE_DEASSERT_CS_WAIT =>
						if CSDelayTicks = 0 then
							State := IDLE;
							CSDelayTicks := CS_DELAY_TICKS - 1;
						end if;
				end case;
			end if;
		end if;
--this needs to be changed back
		StateMachineBusy <= BusClock/= '0';--State /= IDLE -- or CSDelayTicks /= 0;
		--CSPin <= to_stdulogic(State = IDLE);
	end process;

	-- Combinationally compute some SPI master control lines.
	process(ControlLatched) is
		variable OpTypeBit : std_ulogic;
	begin
		if ControlLatched.OpType = READ then
			OpTypeBit := '0';
		else
			OpTypeBit := '1';
		end if;
		case ControlLatched.RegType is
			when SHORT =>
				SPIWidth <= 16;
				SPIWriteData <= "0" & std_ulogic_vector(to_unsigned(ControlLatched.Address, 6)) & OpTypeBit & ControlLatched.WriteData & X"00";
				SPI_write_new <= SPIWriteData(9 downto 2); --added
			when LONG =>
				SPIWidth <= 24;
				SPIWriteData <= "1" & std_ulogic_vector(to_unsigned(ControlLatched.Address, 10)) & OpTypeBit & "0000" & ControlLatched.WriteData;
				SPI_write_new <= SPIWriteData(7 downto 0); --added
		end case;
	end process;

	-- Compute the busy signal to include when Strobe is first asserted.

	Status.Busy <= Control.Strobe;-- or StateMachineBusy;-- or Control.Strobe;
	-- The last eight bits shifted on the bus always appear at the bottom of the SPI read data vector, and are the read data in case of a register read.
	Status.ReadData <= SPIReadData(7 downto 0);

	-- An SPI master transceiver is the bottom level of talking to the MRF24J40.
	--SPI : entity work.SPIMaster(RTL)
	--generic map(
		--MaxWidth => 24,
		--CPhase => 0)
	--port map(
		--Reset => Reset,
		--HostClock => HostClock,
		--BusClock => BusClock,
		--Strobe => SPIStrobe,
		--Width => SPIWidth,
		--WriteData => SPIWriteData,
		--ReadData => SPIReadData,
		--Busy => SPIBusy,
		--ClockOE => ClockOE,
		--MOSIPin => MOSIPin,
		--MISOPin => MISOPin);


SPI: entity work.spi_slave(RTL)
port map(
		clk => HostClock,
		rst => Reset,
		ss => CSPin,
		mosi => MISOPin, -- swapped because now we are using slave and we do not want to change any names because too much work and we are lazy
		miso => MOSIPin,
		miso_en => miso_en,
		sck => BusClock,
		Strobe => SPIStrobe,
--		Width => SPIWidth, --supposed to be done, let's think about this
		busy => SPIBusy,
		done => done,
		din => SPI_write_new,
		dout => SPIReadData(7 downto 0),
		ReceiveInt => ReceiveInt
		
		);
		
		--SPI: entity work.this_spi_slave(RTL)
--port map(
	--	CLK => HostClock,
	--	RST => Reset,
		--CS_N => CSPin,
		--MOSI => MISOPin, -- swapped because now we are using slave and we do not want to change any names because too much work and we are lazy
		--MISO => MOSIPin,
		--SCLK => BusClock,
		--Strobe => SPIStrobe,
--		Width => SPIWidth, --supposed to be done, let's think about this
		--busy => SPIBusy,
		--done => done,
		--DIN => SPI_write_new,
		--DOUT => SPIReadData(7 downto 0),
		--READY => READY,
		--ReceiveInt => ReceiveInt,
		--DOUT_VLD => DOUT_VLD
		--);
end architecture RTL;